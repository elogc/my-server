const express = require('express');
const validate = require('express-validation');
const controller = require('../../controllers/plex.controller');
const { authorize, ADMIN, LOGGED_USER } = require('../../middlewares/auth');


const router = express.Router();

router
  .route('/filebot')
  .get(controller.filebot)

module.exports = router;
