const cp = require('child_process');
const httpStatus = require('http-status');
const { omit } = require('lodash');
const User = require('../models/user.model');
const { handler: errorHandler } = require('../middlewares/error');

/**
 * @public
 */
exports.filebot = async (req, res, next) => {
  try {
    cp.exec('sh /home/elo/Videos/filebot.sh' ,function(err,stdout,stderr){
      console.log(err,stdout,stderr)
      if(err) next(err)
      res.json({result: stdout})
    })

  } catch (error) {
    next(error);
  }
};
